use std::{io::{Read, Write}, net::{SocketAddr, TcpStream}, os::unix::io::{RawFd, AsRawFd}};

use instance::{EpollHandler, HandlerFlow};
use net::ConnectionHandler;

pub mod instance;

pub mod stdin;
pub mod timer;
pub mod net;

pub fn check_errno_error(result: libc::c_int) -> Result<libc::c_int, std::io::Error> {
	if result < 0 {
		Err(std::io::Error::last_os_error())
	} else {
		Ok(result)
	}
}

pub struct EpollConnectionLineCounter {
	stream: TcpStream,
	buffer: Vec<u8>
}
impl EpollConnectionLineCounter {
	pub fn new(stream: TcpStream) -> Result<Self, std::io::Error> {
		stream.set_nonblocking(true)?;
		
		Ok(
			EpollConnectionLineCounter {
				stream,
				buffer: Vec::new()
			}
		)
	}
}
impl EpollHandler for EpollConnectionLineCounter {
	fn fd(&self) -> RawFd {
		self.stream.as_raw_fd()
	}

	fn handle(&mut self, events: u32) -> Result<HandlerFlow, std::io::Error> {
		if (events & (libc::EPOLLERR as u32) != 0) || (events & (libc::EPOLLHUP as u32) != 0) || (events & (libc::EPOLLIN as u32) == 0) {
			return Ok(
				HandlerFlow::Remove
			);
		}

		match self.stream.read_to_end(&mut self.buffer) {
			Err(err) if err.kind() == std::io::ErrorKind::WouldBlock => (),
			Err(err) => return Err(err),
			Ok(0) => {
				eprintln!("Connection closed");
				return Ok(HandlerFlow::Remove)
			},
			Ok(_) => ()
		}

		let mut counter = 0;
		let mut last_newline = None;
		for (i, byte) in self.buffer.iter().copied().enumerate() {
			if byte == b'\n' {
				eprintln!("{:?}: {}", &self.buffer[last_newline.map(|i| i + 1).unwrap_or(0) .. i], counter);

				match writeln!(&self.stream, "{}", counter) {
					Err(err) if err.kind() == std::io::ErrorKind::ConnectionReset => {
						eprintln!("Connection reset by peer");
						return Ok(HandlerFlow::Remove)
					},
					Err(err) => return Err(err),
					Ok(()) => ()
				};
				counter = 0;
				last_newline = Some(i);
			} else {
				counter += 1;
			}
		}

		if let Some(last_newline) = last_newline {
			self.buffer.drain(..= last_newline);
		}

		Ok(HandlerFlow::Continue)
	}
}
impl ConnectionHandler for EpollConnectionLineCounter {
	fn create(stream: TcpStream) -> Result<Self, std::io::Error> {
		Self::new(stream)
	}
}

fn main() {
    let mut instance = instance::EpollInstance::<Box<dyn EpollHandler>>::new().expect("Could not initialize epoll instance");

	let timer = Box::new(
		timer::EpollTimer::new(std::time::Duration::from_secs(3)).expect("Could not initialize epoll timer")
	);
	let stdin = Box::new(
		stdin::EpollStdin::new().expect("Could not initialize epoll stdin")
	);
	let socket = Box::new(
		net::EpollSocket::<EpollConnectionLineCounter>::new(
			SocketAddr::new([0, 0, 0, 0].into(), 12345)
		).expect("Could not initialize poll socket")
	);

	instance.register(libc::EPOLLIN as u32, timer).expect("Could not register epoll timer");
	instance.register(libc::EPOLLIN as u32, stdin).expect("Could not register epoll stdin");
	instance.register(libc::EPOLLIN as u32, socket).expect("Could not register epoll socket");

	loop {
		instance.wait_handle::<64>().expect("Could not wait_handle epoll instance");
	}
}
