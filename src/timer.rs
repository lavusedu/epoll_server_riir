use std::{fs::File, io::Read, os::unix::io::{RawFd, FromRawFd, AsRawFd}};

use crate::instance::{EpollHandler, HandlerFlow};

pub struct EpollTimer {
	file: File
}
impl EpollTimer {
	pub fn new(duration: std::time::Duration) -> Result<Self, std::io::Error> {
		let file = unsafe {
			let fd = crate::check_errno_error(
				libc::timerfd_create(libc::CLOCK_MONOTONIC, libc::TFD_NONBLOCK)
			)?;

			File::from_raw_fd(fd)
		};

		let spec = libc::timespec {
			tv_sec: duration.as_secs() as i64,
			tv_nsec: duration.subsec_nanos() as i64
		};
		let its = libc::itimerspec {
			it_interval: spec,
			it_value: spec
		};

		unsafe {
			crate::check_errno_error(
				libc::timerfd_settime(
					file.as_raw_fd(),
					0,
					&its as *const _,
					std::ptr::null_mut()
				)
			)?;
		}

		Ok(
			EpollTimer { file }
		)
	}
}
impl EpollHandler for EpollTimer {
	fn fd(&self) -> RawFd {
		self.file.as_raw_fd()
	}

	fn handle(&mut self, events: u32) -> Result<HandlerFlow, std::io::Error> {
		if (events & (libc::EPOLLERR as u32) != 0) || (events & (libc::EPOLLHUP as u32) != 0) || (events & (libc::EPOLLIN as u32) == 0) {
			return Ok(
				HandlerFlow::Remove
			);
		}
		
		let mut buffer = [0u8; std::mem::size_of::<u64>()];
		self.file.read_exact(&mut buffer)?;
		
		let value = u64::from_ne_bytes(buffer);
		println!("Timer [{}]: {}", self.fd(), value);

		Ok(
			HandlerFlow::Continue
		)
	}
}