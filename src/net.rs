use std::{net::{SocketAddr, TcpListener, TcpStream}, os::unix::io::{RawFd, AsRawFd}};

use crate::instance::{EpollHandler, HandlerFlow};

pub trait ConnectionHandler: EpollHandler + Sized + 'static {
	fn create(stream: TcpStream) -> Result<Self, std::io::Error>;
}

pub struct EpollSocket<C: ConnectionHandler> {
	socket: TcpListener,
	__boo: std::marker::PhantomData<C>
}
impl<C: ConnectionHandler> EpollSocket<C> {
	pub fn new(address: SocketAddr) -> Result<Self, std::io::Error> {
		let socket = TcpListener::bind(address)?;

		socket.set_nonblocking(true)?;
		
		Ok(
			EpollSocket {
				socket,
				__boo: std::marker::PhantomData
			}
		)
	}
}
impl<C: ConnectionHandler> EpollHandler for EpollSocket<C> {
	fn fd(&self) -> RawFd {
		self.socket.as_raw_fd()
	}

	fn handle(&mut self, events: u32) -> Result<HandlerFlow, std::io::Error> {
		if (events & (libc::EPOLLERR as u32) != 0) || (events & (libc::EPOLLHUP as u32) != 0) || (events & (libc::EPOLLIN as u32) == 0) {
			return Ok(
				HandlerFlow::Remove
			);
		}
		
		let handler = match self.socket.accept().and_then(|(stream, _)| C::create(stream)) {
			Err(err) if err.kind() == std::io::ErrorKind::ConnectionReset => return Ok(HandlerFlow::Continue),
			Err(err) => return Err(err),
			Ok(handler) => handler
		};

		Ok(
			HandlerFlow::RegisterNew(libc::EPOLLIN as u32, Box::new(handler))
		)
	}
}