use std::os::unix::io::RawFd;

use slab::Slab;

pub enum HandlerFlow {
	Continue,
	RegisterNew(u32, Box<dyn EpollHandler>),
	Remove
}

pub trait EpollHandler {
	fn fd(&self) -> RawFd;

	fn handle(&mut self, events: u32) -> Result<HandlerFlow, std::io::Error>;
}
// implement EpollHandler for DerefMut containers, such as Box<H>
impl<H: EpollHandler + ?Sized, C: std::ops::DerefMut<Target = H>> EpollHandler for C {
	fn fd(&self) -> RawFd {
		(**self).fd()
	}
	
	fn handle(&mut self, events: u32) -> Result<HandlerFlow, std::io::Error> {
		(**self).handle(events)
	}
}

pub struct EpollInstance<H: EpollHandler = Box<dyn EpollHandler>> {
	fd: Option<RawFd>,
	handlers: Slab<H>
}
impl<H:EpollHandler> EpollInstance<H> {
	pub fn new() -> Result<Self, std::io::Error> {
		let epoll_fd = unsafe {
			crate::check_errno_error(
				libc::epoll_create1(0)
			)?
		};

		Ok(
			EpollInstance {
				fd: Some(epoll_fd),
				handlers: Slab::new()
			}
		)
	}

	fn get_fd(&self) -> RawFd {
		match self.fd {
			None => panic!("Epoll Instance has already been closed"),
			Some(fd) => fd
		}
	}

	pub fn register(&mut self, events: u32, handler: H) -> Result<usize, std::io::Error> {
		let handler_fd = handler.fd();
		let key = self.handlers.insert(handler);

		let mut event = libc::epoll_event {
			events,
			u64: key as u64
		};

		unsafe {
			crate::check_errno_error(
				libc::epoll_ctl(self.get_fd(), libc::EPOLL_CTL_ADD, handler_fd, &mut event as *mut _)
			)?;
		}

		Ok(key)
	}

	pub fn unregister(&mut self, key: usize) -> Result<H, std::io::Error> {
		let handler = self.handlers.remove(key);

		let mut event = libc::epoll_event {
			events: 0,
			u64: key as u64
		};

		unsafe {
			crate::check_errno_error(
				libc::epoll_ctl(self.get_fd(), libc::EPOLL_CTL_DEL, handler.fd(), &mut event as *mut _)
			)?;
		}

		Ok(handler)
	}

	/// Manually closes the epoll instance.
	///
	/// This can be used to observe the close error.
	/// Otherwise the instance is closed on `Drop`, but that panics on error.
	///
	/// Panics if this instance has been closed already.
	pub fn close(&mut self) -> Result<(), std::io::Error> {
		unsafe {
			crate::check_errno_error(
				libc::close(self.fd.take().unwrap())
			)?;
		}

		Ok(())
	}
}
impl EpollInstance {
	pub fn wait_handle<const MAX_EVENTS: usize>(&mut self) -> Result<(), std::io::Error> {
		let mut events: [libc::epoll_event; MAX_EVENTS] = [libc::epoll_event { events: 0, u64: 0 }; MAX_EVENTS];

		let count = unsafe {
			crate::check_errno_error(
				libc::epoll_wait(
					self.get_fd(),
					events.as_mut_ptr(),
					MAX_EVENTS as i32,
					-1
				)
			)? as usize
		};

		for i in 0 .. count {
			let event = &events[i];

			let key = event.u64 as usize;
			let handler_result = {
				let handler = &mut self.handlers[key];

				handler.handle(event.events)?
			};

			match handler_result {
				HandlerFlow::Continue => (),
				HandlerFlow::RegisterNew(events, new_handler) => {
					self.register(events, new_handler)?;
				}
				HandlerFlow::Remove => {
					self.handlers.remove(key);
				}
			}
		}

		Ok(())
	}
}
impl<H: EpollHandler> Drop for EpollInstance<H> {
	fn drop(&mut self) {
		if self.fd.is_some() {
			self.close().expect("Could not close epoll instance");
		}
	}
}