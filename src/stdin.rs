use std::os::unix::io::{RawFd, AsRawFd};

use crate::instance::{EpollHandler, HandlerFlow};

pub struct EpollStdin {
	stdin: std::io::Stdin,
	buffer: String
}
impl EpollStdin {
	pub fn new() -> Result<Self, std::io::Error> {
		Ok(
			EpollStdin {
				stdin: std::io::stdin(),
				buffer: String::new()
			}
		)
	}
}
impl EpollHandler for EpollStdin {
	fn fd(&self) -> RawFd {
		self.stdin.as_raw_fd()
	}

	fn handle(&mut self, events: u32) -> Result<HandlerFlow, std::io::Error> {
		if (events & (libc::EPOLLERR as u32) != 0) || (events & (libc::EPOLLHUP as u32) != 0) || (events & (libc::EPOLLIN as u32) == 0) {
			return Ok(
				HandlerFlow::Remove
			);
		}
		
		self.stdin.read_line(&mut self.buffer)?;
		println!("Stdin line: {}", self.buffer);
		self.buffer.clear();

		Ok(
			HandlerFlow::Continue
		)
	}
}